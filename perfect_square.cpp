//Write a program to find and print the first perfect square (i*i) whose last two digits
//are both odd

#include <iostream>
using namespace std;
int main() {
	bool perfect_square_flag=false;
	int i=0;
	int j=0;
	long long int test_square=0;
	while (perfect_square_flag==false) {
		//check if the last digit is odd
		test_square=i*i;
		if ((test_square % 2)==1) {
			j=test_square;
			//remove last digit
			j=(j/10);
			if ((j%2)==1) {
				cout << "The first perfect square is:" << test_square << endl;	
				cout << "You can get this perfect square by multiplying twice:" << i <<endl;
				cout << "Value of J is:" << j <<endl;
				perfect_square_flag=true;
			}
		}
		i++;
	
	
	}
	
	return 0;
}
//output is The first perfect square is:131073
//You can get this perfect square by multiplying twice:65537
//Value of J is:13107

//Why this is impossible
//This result is because it is impossible
//This can be determined by looking at the first 100 values. From 50 on the last numbers repeat
//eg. 4**2=16 and 54**2=2916 so if a value with the last two digits being odd isn't reached by 50 it 
//won't be reached for anything else. This being impossible is causing unexpected behavior from
//the program and hence these weird results
